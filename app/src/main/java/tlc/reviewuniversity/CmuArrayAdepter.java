package tlc.reviewuniversity;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class CmuArrayAdepter extends ArrayAdapter<String> {
    private final Context context;
    private final String[] values;

    public CmuArrayAdepter(Context context, String[] values) {
        super(context, R.layout.activity_cmu_array_adepter, values);
        this.context = context;
        this.values = values;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.activity_cmu_array_adepter, parent, false);
        TextView textView = (TextView) rowView.findViewById(R.id.label2);
        ImageView imageView = (ImageView) rowView.findViewById(R.id.logo2);
        textView.setText(values[position]);

        // Change icon based on name
        String s = values[position];

        System.out.println(s);

        if (s.equals("ดร.ธารณ์ ทองงอก")) {
            imageView.setImageResource(R.drawable.cm11);
        } else if (s.equals("ดร.สมเกียรติ อินทสิงห์")) {
            imageView.setImageResource(R.drawable.cm12);
        } else if (s.equals("อาจารย์มาลินี คุ้มสุภา")) {
            imageView.setImageResource(R.drawable.cm13);
        } else if (s.equals("อ.ดร. จันทนา สุทธิจารี")) {
            imageView.setImageResource(R.drawable.cm14);
        }
        else if (s.equals("นายไพโรจน์ วิริยจารี")) {
            imageView.setImageResource(R.drawable.cm15);
        }
        else if (s.equals("อาจารย์บรรเจิด พละการ")) {
            imageView.setImageResource(R.drawable.cm);
        }


        return rowView;
    }
}