package tlc.reviewuniversity;
        import android.app.Activity;
        import android.app.AlertDialog;
        import android.content.Intent;
        import android.os.Bundle;
        import android.view.View;
        import android.widget.Button;
        import android.widget.EditText;
        import android.widget.Toast;

public class AddActivity extends Activity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);


        // btnSave (Save)
        final Button save = (Button) findViewById(R.id.btnSave);
        save.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                // If Save Complete
                if(SaveData())
                {
                    // Open Form Main
                    Intent newActivity;
                    newActivity = new Intent(AddActivity.this,ShowActicvity.class);
                    startActivity(newActivity);
                }
            }
        });


        // btnCancel (Cancel)
        final Button cancel = (Button) findViewById(R.id.btnCancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Open Form Main
                Intent newActivity = new Intent(AddActivity.this,ChulaActivity.class);
                startActivity(newActivity);
            }
        });

    }

    public boolean SaveData()
    {
        // txtMemberID, txtName, txtTel
        final EditText tMemberID = (EditText) findViewById(R.id.txtMemberID);
        final EditText tPerson = (EditText) findViewById(R.id.txtName);
        final EditText tComment = (EditText) findViewById(R.id.txtTel);

        // Dialog
        final AlertDialog.Builder adb = new AlertDialog.Builder(this);
        AlertDialog ad = adb.create();

        // Check MemberID
        if(tMemberID.getText().length() == 0)
        {
            ad.setMessage("Please input [MemberID] ");
            ad.show();
            tMemberID.requestFocus();
            return false;
        }

        // Check Name
        if(tPerson.getText().length() == 0)
        {
            ad.setMessage("Please input [Person] ");
            ad.show();
            tPerson.requestFocus();
            return false;
        }

        // Check Tel
        if(tComment.getText().length() == 0)
        {
            ad.setMessage("Please input [Comment] ");
            ad.show();
            tComment.requestFocus();
            return false;
        }


        Toast.makeText(AddActivity.this,"Add Data Successfully. ",
                Toast.LENGTH_SHORT).show();

        return true;
    }
}