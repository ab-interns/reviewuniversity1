package tlc.reviewuniversity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Chula6 extends AppCompatActivity {
    Button btnadd6;
    Button btncancle6;
    Button btnshow6;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chula6);
    }
    public void setTeachChula6 (View v){
        Button btnadd6 = (Button)findViewById(R.id.btnadd6);
        Intent intent = new Intent(tlc.reviewuniversity.Chula6.this,AddActivity.class);
        startActivity(intent);
    }
    public void setTeachChula (View v){
        Button btncancle5 = (Button)findViewById(R.id.btncancle6);
        Intent intent = new Intent(tlc.reviewuniversity.Chula6.this,ChulaActivity.class);
        startActivity(intent);
    }
    public void setShow (View v){
        Button btnshow6 = (Button)findViewById(R.id.btnshow6);
        Intent intent = new Intent(tlc.reviewuniversity.Chula6.this,ShowActicvity.class);
        startActivity(intent);
    }
}