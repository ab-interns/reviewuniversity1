package tlc.reviewuniversity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Chula3 extends AppCompatActivity {
    Button btnadd3;
    Button btncancle3;
    Button btnshow3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chula3);
    }
    public void setTeachChula3 (View v){
        Button btnadd3 = (Button)findViewById(R.id.btnadd3);
        Intent intent = new Intent(tlc.reviewuniversity.Chula3.this,AddActivity.class);
        startActivity(intent);
    }
    public void setTeachChula (View v){
        Button btncancle3 = (Button)findViewById(R.id.btncancle3);
        Intent intent = new Intent(tlc.reviewuniversity.Chula3.this,ChulaActivity.class);
        startActivity(intent);
    }
    public void setShow (View v){
        Button btnshow3 = (Button)findViewById(R.id.btnshow3);
        Intent intent = new Intent(tlc.reviewuniversity.Chula3.this,ShowActicvity.class);
        startActivity(intent);
    }
}