package tlc.reviewuniversity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Chula5 extends AppCompatActivity {
    Button btnadd5;
    Button btncancle5;
    Button btnshow5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chula5);
    }
    public void setTeachChula5 (View v){
        Button btnadd5 = (Button)findViewById(R.id.btnadd5);
        Intent intent = new Intent(tlc.reviewuniversity.Chula5.this,AddActivity.class);
        startActivity(intent);
    }
    public void setTeachChula (View v){
        Button btncancle5 = (Button)findViewById(R.id.btncancle5);
        Intent intent = new Intent(tlc.reviewuniversity.Chula5.this,ChulaActivity.class);
        startActivity(intent);
    }
    public void setShow (View v){
        Button btnshow5 = (Button)findViewById(R.id.btnshow5);
        Intent intent = new Intent(tlc.reviewuniversity.Chula5.this,ShowActicvity.class);
        startActivity(intent);
    }
}