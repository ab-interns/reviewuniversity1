package tlc.reviewuniversity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;

public class cmu1 extends AppCompatActivity {
    Button btnadd1;
    Button btncancle1;
    Button btnshow1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chula1);
    }
    public void setTeachChula1 (View v){
        Button btnadd2 = (Button)findViewById(R.id.btnadd1);
        Intent intent = new Intent(tlc.reviewuniversity.cmu1.this,AddcmActivity.class);
        startActivity(intent);
    }
    public void setTeachChula (View v){
        Button btncancle1 = (Button)findViewById(R.id.btncancle1);
        Intent intent = new Intent(tlc.reviewuniversity.cmu1.this,ChiangmaiActivity.class);
        startActivity(intent);
    }
    public void setShow (View v){
        Button btnshow1 = (Button)findViewById(R.id.btnshow1);
        Intent intent = new Intent(tlc.reviewuniversity.cmu1.this,ShowActicvity.class);
        startActivity(intent);
    }
}