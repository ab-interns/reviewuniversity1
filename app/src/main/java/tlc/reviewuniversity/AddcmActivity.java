package tlc.reviewuniversity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class AddcmActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addcm);
        final EditText et = (EditText)findViewById(R.id.edtReview);

        Button b = (Button)findViewById(R.id.btGo);



        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AddcmActivity.this,ShowcmActivity.class);
                intent.putExtra("Review",et.getText().toString());

                startActivity(intent);
            }
        });
    }
}
