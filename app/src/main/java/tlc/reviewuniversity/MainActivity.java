package tlc.reviewuniversity;

import android.app.ListActivity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

public class MainActivity extends ListActivity {

    static final String[] MOBILE_OS =
            new String[]{"Chulalongkorn University", "Chaingmai University", "Rajamangala University of Technology Lanna","Mahidol University","Kasetsart University","Khonkean University"};

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setListAdapter(new UniversityArrayAdepter(this, MOBILE_OS));

    }


    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {

        if (position == 0) {
            Intent myintent = new Intent(getListView().getContext(), ChulaActivity.class);
            startActivityForResult(myintent, 0);
        }
        if (position == 1) {
            Intent myintent = new Intent(getListView().getContext(), ChiangmaiActivity.class);
            startActivityForResult(myintent, 1);
        }
        if (position == 2) {
            Intent myintent = new Intent(getListView().getContext(), ChulaActivity.class);
            startActivityForResult(myintent, 2);
        }
        if (position == 3) {
            Intent myintent = new Intent(getListView().getContext(), ChulaActivity.class);
            startActivityForResult(myintent, 3);
        }
        if (position == 4) {
            Intent myintent = new Intent(getListView().getContext(), ChulaActivity.class);
            startActivityForResult(myintent, 4);
        }

    }}





