package tlc.reviewuniversity;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class UniversityArrayAdepter extends ArrayAdapter<String> {
    private final Context context;
    private final String[] values;

    public UniversityArrayAdepter(Context context, String[] values) {
        super(context, R.layout.activity_university_array_adepter, values);
        this.context = context;
        this.values = values;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.activity_university_array_adepter, parent, false);
        TextView textView = (TextView) rowView.findViewById(R.id.label);
        ImageView imageView = (ImageView) rowView.findViewById(R.id.logo);
        textView.setText(values[position]);

        // Change icon based on name
        String s = values[position];

        System.out.println(s);

        if (s.equals("Chulalongkorn University")) {
            imageView.setImageResource(R.drawable.jula);
        } else if (s.equals("Chaingmai University")) {
            imageView.setImageResource(R.drawable.cm);
        } else if (s.equals("Rajamangala University of Technology Lanna")) {
            imageView.setImageResource(R.drawable.lanna);
        } else if (s.equals("Mahidol University")) {
            imageView.setImageResource(R.drawable.mahidal);
        }
        else if (s.equals("Kasetsart University")) {
            imageView.setImageResource(R.drawable.ku);
        }
        else if (s.equals("Khonkean University")) {
            imageView.setImageResource(R.drawable.khonkean);
        }


        return rowView;
    }
}