package tlc.reviewuniversity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class ShowcmActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_showcm);

        TextView reviewView=(TextView)findViewById(R.id.edReview);
        reviewView.setText(getIntent().getExtras().getString("Review"));

    }
}
