package tlc.reviewuniversity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Chula4 extends AppCompatActivity {
    Button btnadd4;
    Button btncancle4;
    Button btnshow4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chula4);
    }
    public void setTeachChula4 (View v){
        Button btnadd4 = (Button)findViewById(R.id.btnadd4);
        Intent intent = new Intent(tlc.reviewuniversity.Chula4.this,AddActivity.class);
        startActivity(intent);
    }
    public void setTeachChula (View v){
        Button btncancle4 = (Button)findViewById(R.id.btncancle4);
        Intent intent = new Intent(tlc.reviewuniversity.Chula4.this,ChulaActivity.class);
        startActivity(intent);
    }
    public void setShow (View v){
        Button btnshow4 = (Button)findViewById(R.id.btnshow4);
        Intent intent = new Intent(tlc.reviewuniversity.Chula4.this,ShowActicvity.class);
        startActivity(intent);
    }
}