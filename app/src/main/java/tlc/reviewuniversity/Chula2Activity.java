package tlc.reviewuniversity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;



public class Chula2Activity extends AppCompatActivity {
    Button btnadd2;
    Button btncancle2;
    Button btnshow2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chula2);
    }
    public void setTeachChula2 (View v){
        Button btnadd2 = (Button)findViewById(R.id.btnadd2);
        Intent intent = new Intent(Chula2Activity.this,AddActivity.class);
        startActivity(intent);
    }
    public void setTeachChula (View v){
        Button btncancle2 = (Button)findViewById(R.id.btncancle2);
        Intent intent = new Intent(Chula2Activity.this,ChulaActivity.class);
        startActivity(intent);
    }
    public void setShow2 (View v){
        Button btnshow2 = (Button)findViewById(R.id.btnshow2);
        Intent intent = new Intent(Chula2Activity.this,Show2.class);
        startActivity(intent);

    }
}