package tlc.reviewuniversity;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;


class ChulaArrayAdapter extends ArrayAdapter<String> {
    private final Context context;
    private final String[] values;

    public ChulaArrayAdapter(Context context, String[] values) {
        super(context, R.layout.activity_chula_array_adepter, values);
        this.context = context;
        this.values = values;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.activity_chula_array_adepter, parent, false);
        TextView textView = (TextView) rowView.findViewById(R.id.label1);
        ImageView imageView = (ImageView) rowView.findViewById(R.id.logo1);
        textView.setText(values[position]);

        // Change icon based on name
        String s = values[position];

        System.out.println(s);

        if (s.equals("อาจารย์สมคิด รอดเมือง วิชาพื้นฐานวิศวกรรมศาสตร์")) {
            imageView.setImageResource(R.drawable.chula1);
        } else if (s.equals("อาจารพงษ์ธร จรัญญากรณ์ วิชาคณิตศาสตร์")) {
            imageView.setImageResource(R.drawable.chula2);
        } else if (s.equals("อาจารย์ธนาวรรณ จันทรัตนไพบูลย์  วิชาเคมี ")) {
            imageView.setImageResource(R.drawable.chula3);
        } else if (s.equals("อาจารย์สาธิต วงศ์ประทีป วิชาสถิติ")) {
            imageView.setImageResource(R.drawable.chula4);
        }
        else if (s.equals("อาจารย์วิชัย เยี่ยงวีรชน  วิชากลศาสตร์")) {
            imageView.setImageResource(R.drawable.chula5);
        }
        else if (s.equals("อาจารย์มะลิ มีเงิน  วิชาคอมพิวเตอร์")) {
            imageView.setImageResource(R.drawable.chula6);
        }


        return rowView;
    }
}